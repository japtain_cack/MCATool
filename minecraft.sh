#!/bin/bash
#
# chkconfig: 345 99 01
# description: Minecraft Server
#
### SETTINGS
#
# Server Name
SERVERNAME='spigot'
# Linux Username
USERNAME='minecraft'
# RCON Password
RCONPASS='password'
# Server Root Folder
MCPATH="/usr/local/minecraft"
# Server Jar File
SERVERJAR="${MCPATH}/${SERVERNAME}.jar"
# Server Java Command
EXECUTE="java -jar ${SERVERJAR}"
# MCRCON path to executable
MCRCON="/usr/local/minecraft/mcrcon/mcrcon"
# Minecraft Worlds
WORLD="Pirates_of_the_Pancreas"
# Backup Folder
BACKUPPATH="${MCPATH}/backups"
# Restore Folder
RESTOREPATH="/usr/local/minecraft/restore"
#Temporary dir
TMPDIR="/usr/local/minecrafttmp"
# Colors
reset="\e[0m";
black="\e[1;30m";
blue="\e[1;34m";
cyan="\e[1;36m";
green="\e[1;32m";
orange="\e[1;33m";
purple="\e[1;35m";
red="\e[1;31m";
violet="\e[1;35m";
white="\e[1;37m";
yellow="\e[1;33m";

# Run as user defined above
ME=`whoami`
as_user() {
        if [ "${ME}" == "${USERNAME}" ]
        then
                bash -c "${1}"
        else
                su - ${USERNAME} -c "${1}"
        fi
}

start() {
        if pgrep -u ${USERNAME} -f ${SERVERJAR} > /dev/null
        then
                say "${cyan}${SERVERJAR}${reset} is already ${green}running${reset}."
        else
                say "Starting ${cyan}${SERVERJAR}${reset}..."

		# Start minecraft as a background process
                as_user "cd ${MCPATH} && ${EXECUTE} &"

                # Wait 5 seconds to start up before checking
                sleep 5
                if pgrep -u ${USERNAME} -f ${SERVERJAR} > /dev/null
                then
                        # Print output
                        tail -n $[`wc -l "${MCPATH}/logs/latest.log" | awk '{print $1}'`] "${MCPATH}/logs/latest.log"
                else
                        say "Could ${red}not start ${cyan}${SERVERJAR}${reset}."
                fi
        fi
}

stop() {
        if pgrep -u ${USERNAME} -f ${SERVERJAR} > /dev/null
        then
		# Find last position in log
                pre_log_len=`wc -l "${MCPATH}/logs/latest.log" | awk '{print $1}'`
                say "${cyan}${SERVERJAR}${reset} is ${green}running${reset}. ${red}Stopping${reset} server..."

		# Send stop command to Minecraft server
                send stop

                # Wait 10 seconds to finish stopping
                sleep 10

                # Print output
                tail -n $[`wc -l "${MCPATH}/logs/latest.log" | awk '{print $1}'`-${pre_log_len}] "${MCPATH}/logs/latest.log"
        else
                say "${cyan}${SERVERJAR}${reset} is ${red}not running${reset}..."
        fi
}

kill() {
	if pgrep -u ${USERNAME} -f ${SERVERJAR} > /dev/null
        then
		# Find last position in log
                pre_log_len=`wc -l "${MCPATH}/logs/latest.log" | awk '{print $1}'`
                say "${cyan}${SERVERJAR}${reset} is ${green}running${reset}. ${red}Stopping${reset} server..."

		# Kill all process running under the user set above
                as_user "pkill -u minecraft"

                # Wait 10 seconds to finish stopping
                sleep 10

                # Print output
                tail -n $[`wc -l "${MCPATH}/logs/latest.log" | awk '{print $1}'`-${pre_log_len}] "${MCPATH}/logs/latest.log"
        else
                say "${cyan}${SERVERJAR}${reset} is ${red}not running${reset}..."
        fi
}

status() {
        if pgrep -u ${USERNAME} -f ${SERVERJAR} > /dev/null
        then
                say "${cyan}${SERVERJAR}${reset} is ${green}running${reset}." \
                  "${yellow}*** For complete log run: less ${MCPATH}/logs/latest.log ***${reset}"
                tail -n 10 ${MCPATH}/logs/latest.log
        else
                say "${cyan}${SERVERJAR}${reset} is ${red}NOT running${reset}." \
                  "${yellow}*** For complete log run: less ${MCPATH}/logs/latest.log ***${reset}"
                tail -n 10 ${MCPATH}/logs/latest.log
        fi
}

send() {
        if pgrep -u ${USERNAME} -f ${SERVERJAR} > /dev/null
        then
                # Send user defined RCON command
                for var in "$@"
			do
			    ${MCRCON} -H 127.0.0.1 -p ${RCONPASS} "${var}"
			done
        else
                say "${cyan}${SERVERJAR}${reset} is ${red}not running${reset}..."
        fi
}

terminal() {
        if pgrep -u ${USERNAME} -f ${SERVERJAR} > /dev/null
        then
                # Open minecraft RCON terminal
                ${MCRCON} -t -H 127.0.0.1 -p ${RCONPASS}
        else
                say "${cyan}${SERVERJAR}${reset} is ${red}not running${reset}..."
        fi
}

backup() {
	case "${1}" in
		hourly)
			say "Performing hourly backup on ${cyan}${WORLD}${reset}..."

			# Save all then disable server auto-saving to unlock files
			send save-on
			sleep 3
			send save-all
			sleep 3
			send save-off

			# Execute hourly backup command
			as_user "mkdir -p ${BACKUPPATH}/hourly"
			as_user "rsync -a --exclude 'backups/' ${MCPATH}/* ${BACKUPPATH}/hourly" && say "${green}Backup complete${reset}."

			# Re-enable auto-saving for world
			send save-on
			;;
		daily)
			say "Performing daily backup on ${cyan}${WORLD}${reset}..."

			# Save all then disable server auto-saving to unlock files
			send save-on
			sleep 3
			send save-all
			sleep 3
			send save-off

			# Execute daily backup command
			as_user "mkdir -p ${BACKUPPATH}/daily"
			as_user "rsync -a --delete --exclude 'backups/' ${MCPATH}/* ${BACKUPPATH}/daily" && say "${green}Backup complete${reset}."

			# Re-enable auto-saving for world
			send save-on
			;;
		weekly)
			say "Performing weekly backup on ${cyan}${WORLD}${reset}..."

			# Execute weekly backup command
			as_user "mkdir -p ${BACKUPPATH}/weekly"
			as_user "rsync -a --delete ${BACKUPPATH}/daily/* ${BACKUPPATH}/weekly" && say "${green}Backup complete${reset}."
			;;
		monthly)
			say "Performing monthly backup on ${cyan}${WORLD}${reset}..."

			# Remove old monthly backup
			as_user "find ${BACKUPPATH}/monthly/* -mtime +31 -exec rm -f {} \;"

			# Execute monthly backup command
			as_user "tar -cjf ${BACKUPPATH}/monthly/$(date +%Y%m).tar.bz2 -C ${BACKUPPATH}/daily ." && say "${green}Backup complete${reset}."
			;;
		*)
			say "${yellow}Usage: ${0} backup {hourly|daily|weekly|monthly}${reset}"
	                exit 1
			;;
	esac
}

restore() {
	if pgrep -u ${USERNAME} -f ${SERVERJAR} > /dev/null
	then
		say "${red}You probably shouldn't restore a running server...${reset}" \
		"Try shutting it down first :)"
		exit 1
	else
		read -rp "Restore Minecraft from backup? [yes/n] : " QUERY
		if [ "${QUERY}" == "yes" ]; then
			case "${1}" in
				 daily)
					RESTOREFROM="daily"
					say "${green}Starting daily restore${reset}" "this can take some time." "Please wait..."

					# Create directory and restore to RESTOREPATH
					as_user "mkdir -p ${RESTOREPATH}"
					as_user "rsync -a --delete ${BACKUPPATH}/daily/* ${RESTOREPATH}" \
					  && say "${green}Restored from ${violet}${BACKUPPATH}/daily${reset}" \
					    "${green}to--> ${violet}${RESTOREPATH}${reset}" \
					    "Has now completed."
		                        ;;
		                weekly)
					RESTOREFROM="weekly"
					say "${green}Starting weekly restore${reset}" "this can take some time." "Please wait..."

					# Create directory and restore to RESTOREPATH
					as_user "mkdir -p ${RESTOREPATH}"
					as_user "rsync -a --delete ${BACKUPPATH}/weekly/* ${RESTOREPATH}" \
					  && say "${green}Restored from ${violet}${BACKUPPATH}/weekly${reset}" \
					    "${green}to--> ${violet}${RESTOREPATH}${reset}" \
					    "Has now completed."
		                        ;;
		                monthly)
					RESTOREFROM="monthly"
					say "${green}Starting monthly restore${reset}" "this can take some time." "Please wait..."

					# Extract monthly to tmp directory, restore to RESTOREPATH, and cleanup tmp dir
					as_user "mkdir -p ${TMPDIR}/restore/monthly"
					as_user "tar -xjf $(ls -t ${BACKUPPATH}/monthly/*.tar.bz2 | head -n 1) --directory ${TMPDIR}/restore/$RESTOREFROM" && say "Extraction to tmp dir complete..."
		                        as_user "rsync -a --delete ${TMPDIR}/restore/monthly/* ${RESTOREPATH}" \
		                          && say "${green}Restore from ${violet}${TMPDIR}/restore/monthly${reset}" \
					    "${green}to--> ${violet}${RESTOREPATH}${reset}" \
					    "Has now completed."
					as_user "rm -rf ${TMPDIR}/restore" && say "Cleaned up tmp dir..."
		                        ;;
		                *)
		                        say "${yellow}Usage: ${0} restore {daily|weekly|monthly}${reset}"
		                        exit 1
		                        ;;
		        esac
		elif [ "${QUERY}" == "n" ]; then
			say "${yellow}No changes made" "Exiting${reset}..."
			exit 0
		else
			say "${red}You must type (yes) or (n).${reset}"
			exit 1
		fi
	fi
}

say() {
	echo
	echo -e "${orange}========================================${reset}"
	for var in "$@"
		do
		    echo -e "${var}"
		done
	echo -e "${orange}========================================${reset}"
	echo

}

#Start-Stop here
case "${1}" in
        start)
                start
                ;;
        stop)
                stop
                ;;
	kill)
		kill
		;;
        restart)
                stop
                start
                ;;
        status)
                status
                ;;
        send)
                send ${2}
                ;;
        terminal)
                terminal
                ;;
	backup)
		backup ${2}
		;;
	restore)
		restore ${2}
		;;
        *)
                say "${yellow}Usage: ${0} {start|stop|kill|restart|status|send|terminal|backup|restore}${reset}"
                exit 1
                ;;
esac

exit 0
