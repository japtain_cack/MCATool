# Minecraft-Scripts
Run the Minecraft server jar as a linux systemd/chkconfig service, backups, RCON, and more!

## Usage:
* systemd
 * `systemctl start|stop|restart minecraft.service`
* minecraft.sh
 * `minecraft start|stop|kill|restart|backup|restore|send|terminal`
  * `minecraft backup daily|weekly|monthly`
  * `minecraft restore daily|weekly|monthly`

## Dependencies:
* Download/install spigot buildtools: https://hub.spigotmc.org/jenkins/job/BuildTools/
* Download/compile mcrcon: https://github.com/Tiiffi/mcrcon
* Install rsync for backup functionality.

## Installation
* Create a dedicated Minecraft service account: #`adduser -m minecraft`
* Clone this repo to your minecraft user's home dir: #`git clone https://github.com/Jedimaster0/Minecraft-Scripts.git`
* Navigate to your minecraft installation dir: #`cd /usr/local/minecraft`
* Ensure the minecraft directory has the proper perms: #`chown minecraft:minecraft -r /usr/local/minecraft`
* I like to crate a symlink to the spigot jar so I just have to reference "spigot.jar": #`ln -s /usr/local/minecraft/spigot-1.9.4.jar /usr/local/minecraft/spigot.jar`
 * Update this when updating spigot since the filename may change.
* Copy the minecraft.sh script to your minecraft dir.
* (Optional)
 * Enable in systemd or crontab to start on server boot.

## minecraft.service:
* Copy this script to your Minecraft directory.
* Create the links to "install" the service: #`ln -P /usr/local/minecraft/minecraft.service /usr/lib/systemd/system/minecraft.service`
 * Systemd doesn't like symbolic links when you enable a service for some reason, so we use a hard link (-P).
* Reload the systemctl daemon: #`systemctl daemon-reload`
* Check your service: #`systemctl start minecraft.service`
* Make sure the status shows "Running": #`systemctl status minecraft.service`
* Enable your service to start upon system boot: #`systemctl enable minecraft.service`

## minecraft.sh:
* Copy this script to your Minecraft directory.
* Create a symbolic link to your `/etc/init.d` directory: #`ln -s /usr/local/minecraft/minecraft.sh /etc/init.d/minecraft`
* Create a symbolic link to your `/bin` directory: #`ln -s /usr/local/minecraft/minecraft.sh /bin/minecraft`
 * Lets you use `minecraft` from anywhere.
* (If you use chkconfig instead of systemd)
 * Add the script to chkconfig: #`chkconfig --add minecraft`
 * Verify that it's on: #`chkconfig --list`
